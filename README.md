# Gestion de l'accès depuis l'extérieur du cluster avec Kubernetes Ingress

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

Nous allons parler de la gestion de l'accès depuis l'extérieur du cluster avec Kubernetes Ingress. Voici ce que nous allons couvrir dans cette leçon :

1. Qu'est-ce qu'un Ingress ?
2. Les contrôleurs d'Ingress
3. Le routage vers un service avec un Ingress
4. Le routage vers un service avec un port nommé
5. Démonstration pratique

# Qu'est-ce qu'un Ingress ?

Un Ingress est un objet Kubernetes qui gère l'accès externe aux services dans notre cluster. Un Ingress peut fournir plus de fonctionnalités qu'un simple service NodePort. Les services NodePort fournissent un accès externe, mais les Ingress permettent de faire beaucoup plus, comme fournir une terminaison SSL, un équilibrage de charge avancé ou un hébergement virtuel basé sur des noms. Il s'agit donc d'une manière plus sophistiquée de fournir un accès à nos ressources de cluster depuis l'extérieur.

>![Alt text](img/image.png)


# Les contrôleurs d'Ingress

Les objets Ingress ne font rien par eux-mêmes. Pour qu'ils fassent quelque chose, vous devez installer un ou plusieurs contrôleurs d'Ingress. Il existe une variété de contrôleurs d'Ingress disponibles, qui implémentent différentes méthodes pour fournir un accès externe à vos services. Cependant, le processus de configuration d'un contrôleur d'Ingress et la gestion de tout cela dépassent le cadre de la certification Certified Kubernetes Administrator (CKA). Si vous êtes intéressé par cela, consultez la documentation de Kubernetes. Pour l'instant, il est important de comprendre ce que sont les contrôleurs d'Ingress et ce qu'ils font.

# Le routage vers un service avec un Ingress

Les Ingress fonctionnent en tandem avec les services. Les Ingress commencent par définir un ensemble de règles de routage. Chaque règle de routage a des propriétés qui déterminent quelles demandes entrantes seront gérées par cette règle. Ensuite, la règle a un ensemble de chemins, chacun ayant un backend, et les demandes correspondant à un chemin particulier seront acheminées vers ce backend associé. Voici à quoi ressemble un Ingress :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
spec:
  rules:
  - http:
      paths:
      - path: /somepath
        pathType: Prefix
        backend:
          service:
            name: my-service
            port:
              number: 80
```

Dans cet exemple, nous avons une seule règle définie, et cette règle définit un chemin qui est `/somepath`. Si nous recevons une demande à `http://<endpoint>/somepath`, cette règle unique va s'appliquer à cette demande et elle sera acheminée vers un service backend appelé `my-service` sur le port 80.

# Le routage vers un service avec un port nommé

Nous pouvons également router vers des ports nommés en utilisant des Ingress. Si un service utilise un port nommé, un Ingress peut utiliser le nom du port au lieu du numéro pour choisir le port vers lequel il routera sur le service. Voici un exemple de service :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  slector:
  app: MyApp
  ports:
  - name: web
    protocol: TCP
    targetPort: 8080
```

Et dans notre Ingress :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
spec:
  rules:
  - http:
      paths:
      - path: /somepath
        pathType: Prefix
        backend:
          service:
            name: my-service
            port:
              name: web
```

Au lieu de spécifier le numéro de port 80, nous mettons le nom du port, et l'Ingress déterminera automatiquement quel port utiliser.
Cela nous permet par exemple d'avoir le numéro de port spécifié à un seul endroit et de pouvoir changer celui-ci au besoin sans avoir a modifier plusieurs fichiers.

# Démonstration pratique

Voyons à quoi cela ressemble dans notre cluster Kubernetes.

Une fois connecté au Control Node.

1. **Lister les services existants** :

```bash
kubectl get svc
```

   Cela nous montrera les services existants, comme `svc-clusterip` que nous avons créé dans une leçon précédente.

2. **Créer un Ingress** :

   Créez un fichier YAML pour l'Ingress :

   ```bash
   vi my-ingress.yml
   ```

   Ajoutez ce contenu :

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: my-ingress
spec:
    rules:
    - http:
        paths:
        - path: /somepath
        pathType: Prefix
        backend:
            service:
            name: svc-clusterip
            port:
                number: 80
```

   Sauvegardez et créez l'Ingress :

```bash
kubectl apply -f my-ingress.yml
```

>![Alt text](img/image-1.png)


3. **Vérifier l'Ingress** :

```bash
kubectl describe ingress my-ingress
```

>![Alt text](img/image-2.png)


   Vous devriez voir les backends de l'Ingress et vous assurer qu'il route vers le bon service.

4. **Modifier le service pour utiliser un port nommé** :

   Éditez le service `svc-clusterip` :

   ```bash
   kubectl edit svc svc-clusterip
   ```

   Ajoutez un nom de port :

   ```yaml
   ports:
   - port: 80
     name: http
   ```

   Appliquez la modification :

```bash
kubectl apply -f svc-clusterip.yml
```

>![Alt text](img/image-3.png)

5. **Modifier l'Ingress pour utiliser le port nommé** :

   Éditez le fichier `my-ingress.yml` :

   ```yaml
   port:
     name: http
   ```

   Appliquez la modification :

```bash
kubectl apply -f my-ingress.yml
```

   Vérifiez à nouveau l'Ingress pour vous assurer qu'il fonctionne correctement :

```bash
kubectl describe ingress my-ingress
```

>![Alt text](img/image-4.png)

# Conclusion

Dans cette leçon, nous avons parlé de ce que sont les Ingress, des contrôleurs d'Ingress, du routage vers un service avec un Ingress, du routage vers un service avec un port nommé, et nous avons fait une démonstration pratique. C'est tout pour cette leçon. À la prochaine !



# Reférences



https://kubernetes.io/docs/concepts/services-networking/ingress/

https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/